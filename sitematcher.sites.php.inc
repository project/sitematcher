<?php

/**
 * @file
 * Detects the site ID related to the current request
 * and adds a suitable definition to the $sites array.
 */

include dirname( __FILE__ ) . '/sitematcher.class.inc';
$sitematcher = new DrupalSiteMatcher();
$sites[$sitematcher->internals['sitename']] = $sitematcher->internals['site'];
