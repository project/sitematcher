<?php

/**
 * @file
 * Example database settings include for DrupalSiteMatcher configuration.
 *
 * See example.settings.inc for detailed documentation on each config option.
 */

/**
 * Global database settings apply to every sites from our sites example.
 */
$data['+'] = array(
  'settings' => array(
    'databases' => array (
      'default' => array (
        'default' => array (
          'prefix' => '',
          'username' => 'db_user',
          'password' => 'db_pass',
          'host' => 'localhost',
          'port' => '',
          'driver' => 'mysql',
        ),
      ),
    ),
  ),
);
