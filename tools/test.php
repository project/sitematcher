<?php

/**
 * @file
 * Debugging helper file template. Use it to debug sitematcher
 * on your local CLI.
 */

// You may want to fix this path to absolutely reflect your Drupal base folder.
define('DRUPAL_ROOT', dirname( __FILE__ ) . '/../../../../..');

// You may also want to adjust the following to the hostname in question.
define('SITE_NAME', 'www.example.com');

include DRUPAL_ROOT . '/sites/all/libraries/sitematcher/sitematcher.class.inc';

$test = new DrupalSiteMatcher(SITE_NAME);
var_dump($test->settings);
var_dump($test->conf);
var_dump($test->internals);
