------------------------------------------------------------------------------
  sitematcher library Readme
  http://drupal.org/project/sitematcher

  By David Herminghaus, http://david.herminghaus.de/
  Sponsored by HiRN GmbH, http://www.hirn.it/
------------------------------------------------------------------------------

INTRODUCTION
============

Sitematcher is not literally a Drupal module, but a handy API tool for Drupal
multisite environments. Features:

* Eliminates repeating code/config at bootstrap level
* Regex based hostname-to-siteID blacklisting and matching 
* Dynamic $sites, $settings and $conf generation
* Improves versioning of multisite setups

Sitematcher aims to reduce non-versioned and also copy & paste overhead at
bootstrap level by replacing file- and folder-based configuration with
configuration arrays. This reduces the common side effects of repeated code,
and also allows for versioning most of your bootstrap-level configuration files
without revealing secrets (like database configuration).

Sitematcher also facilitates multi-environment deployment (e. g. dedicated
settings for production, staging and development), with again no duplicate
code/config at any point (except for "known issues" below).


CONTENTS
========

1. ABOUT
2. TECHNICAL
3. QUICK REFERENCE
4. TOOLS, DEBUGGING AND IN-DEPTH
5. KNOWN ISSUES


1. ABOUT
========

A common issue when dealing with Drupal multisite setups is the overhead
in configuring each host environment, maintaining individual sites.php and
settings.php files for any of your multisites, on any host. Inspired by this
http://netzaffe.de/book/export/html/707 nice and smart article, sitematcher
tries to automate there's suggestions (and a bit more).

The basic idea is to prepare global configuration snippets ("sections"),
each of which can be included (optionally based on regex conditions matched
against the actual host name from the request). The actually matching
sections are merged into configuration arrays which can then be used
on request, e.g. by including the sitematcher class in a central settings.php
and then extracting/merging its property arrays into the current variable scope.
Think of the workflow like something somehow similar to e.g. MS' GPO concept.

Sitematcher can actually deliver any setting you may need in settings.php,
sites.php and probably anywhere else. Just include the class file,
or one of the shorthand wrappers, put your individual optional data includes
in /sites/default/sitematcher/includes and go.


2. TECHNICAL
============

Site detection is done in two (three) steps.

1. Internal site name and folder detection
First, the tool tries to identify which site is actually being called.
To achieve this, 

1. the actual content of $_SERVER['HTTP_HOST'] or
2. a command line argument "--uri" (drush related) or
3. a command line argument "@alias" (drush related)

is evaluated as the request hostname and then reassembled like in Drupal
bootstrap (i.e., optionally prepending any non-80 port like a subdomain).
Then it is matched against the host_regex pattern provided in your
sites.settings.inc (see below) and the first match (if any)
is stored as $sitematcherInstance->internals['site'].
If no definition matches, the site name falls back to "default".

2. Site specific settings
DRUPAL_ROOT/sites/default/sitematcher/includes is browsed for a 
"sites.settings.inc" file containing section definitions. The section keyed
with the site id detected in #1 is parsed into the basic configuration arrays:

	a. $sitematcherInstance->settings
	   Any key of this array should correspond with what you usually set
	   directly in settings.php, e.g. "databases", "cookie_comain" etc.
	b. $sitematcherInstance->internals
	   Any data you would rather not merge into settings.php (mostly
	   includes configuration, matching regex patterns etc.
	c. $sitematcherInstance->conf
	   Lately, more and more addons tend to allow for configuration via
	   settings.php, using the $conf array. This section can e.g. be merged
	   into $conf.

If no definition is found for the given site ID, the "*" section is looked
up and applied (if configured).

Finally, any content of the optional "+" section is merged to the existing
configuration. This means, you may place every globally repeating setting
or include into the "+" section in order to avoid copy & paste definitions.

3. Additional settings includes.
By adding strings to the $section['internals']['includes'] array, you can
tell sitematcher to load, match and apply unlimited additional definitions
with regex filters, fallbacks and globals. As this even works recursively,
you will not need to define any settings/configurations detail more than once.
Also, you can easily manage even large multisite without repeating yourself
more than necessary (i.e. never).
Name pattern:

	$section['internals']['includes']['db']
	
will refer to a custom include named
	
	db.settings.inc
	
and so on.


3. QUICK REFERENCE
==================

1. Extract the "module" tarball into DRUPAL_ROOT/sites/all/libraries,
   so that sitematcher.class.inc is located at
   DRUPAL_ROOT/sites/all/libraries/sitematcher/sitematcher.class.inc
2. Copy the code snippet from snippets/snippet.settings.php.inc into your
   sites/default/settings.php (carefully choose the position regarding
   mutual overwriting!).
3. Copy the code snippet from snippets/snippet.sites.php.inc
   into your sites.php. The include may be combined with existing hard-coded
   entries, it only adds to the $sites array. However, keep mutual overwriting
   in mind.
4. Use examples/example.settings.inc as a template for your sites configuration
   and save the result in
   DRUPAL_ROOT/sites/default/sitematcher/includes/sites.settings.inc
   You will also find much inline documentation in that example file.
5. Optionally create custom includes based on examples/example.settings.inc
   and save the results to the said folder, following the naming convention
   from the "TECHNICAL" section above.


4. TOOLS, DEBUGGING AND IN-DEPTH
================================

* Use the example test.php as a starting point for debugging your own 
  extensions.
* Since sitematcher is included in every bootstrap, the DrupalSiteMatcher class
  can be used from within Drupal, for debugging or whatever .
* Find practical usage examples and in-depth coverage at 
  https://www.drupal.org/node/2614164.

5. KNOWN ISSUES
===============

Drupal core defines some conventions sitematcher cannot ignore without breaking
functionality. However, they can be worked around in an acceptable way.

Actually, Drupal core demands a "settings.php" file to exist in a site
folder in certain situations. Find related information on the issue at
https://www.drupal.org/node/2605632.

As you unlikely want to start with copy & pasting, there are two simple
workarounds:

1.) Symlinks
    Depending on your server's file system, you may add a symlink to each
    site folder, pointing to '../default/settings.php'. It may save actual
    file access, however you never know which file checks are made by,
    e. g., a contrib module.
2.) Use the included per-site template
    Copy the templates/per-site.settings.php to each site folder, then rename
    it to "settings.php" and change its permissions to be read-only.
    This file will simply include the central settings file. It thus needs no
    further lifecycle attention, as far as I am aware.
