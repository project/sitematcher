<?php

/**
 * @file
 * Generates sitematcher configuration and settings values
 * and merges them to the settings.php scope.
 */

require_once dirname( __FILE__ ) . '/sitematcher.class.inc';
$sitematcher = new DrupalSiteMatcher();
extract($sitematcher->settings);
$conf = array_merge($conf, $sitematcher->conf);
