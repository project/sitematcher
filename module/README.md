# sitematcher tracking module

Sitematcher is actually a bootstrap module, not providing any "in-Drupal"
functionality. However, you may want to keep track of updates and fixes.

It may also happen that some monitoring and error reporting is added to
the admin backend.

To take advantage of this, you need to copy the "sitematcher" subfolder to your
sites/all/modules or sites/$sitename/modules folder and enable the sitematcher
module. It is part of the "Multisite" group.
